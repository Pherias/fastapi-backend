from pydantic import BaseModel
from typing import Optional

class UserCreate(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str

class User(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str

class Login(BaseModel):
    email: str
    password: str

class TokenData(BaseModel):
    email: Optional[str] = None
