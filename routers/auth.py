from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from datetime import datetime, timedelta
from jose import jwt
import os;

from models import Login
from dependencies import get_current_user
from database import UserModel, SessionLocal

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

SECRET_KEY = os.getenv("SECRET_KEY")
ALGORITHM = "HS256"


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def create_jwt_token(data: dict):
    
    to_encode = data.copy()
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    return encoded_jwt

@router.post("/token", response_model=dict)
async def login_for_access_token(login_data: Login, db: SessionLocal = Depends(get_db)):
    
    user = db.query(UserModel).filter(UserModel.email == login_data.email, UserModel.password == login_data.password).first()

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

    token_data = {
        'user': {
            'id': user.id,
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
        },
        'exp': datetime.utcnow() + timedelta(days=100),
    }

    access_token = create_jwt_token(token_data)

    return {"access_token": access_token, "token_type": "bearer"}
