from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select

from dependencies import get_current_user
from models import User, UserCreate

from database import UserModel, SessionLocal
from typing import List
from sqlalchemy import delete

router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.post("/register", response_model=User)
async def register_user(user: UserCreate, db: SessionLocal = Depends(get_db)):
    query = select(UserModel).where(UserModel.email == user.email)
    
    existing_user = db.execute(query).scalar_one_or_none()
    
    if existing_user:
        raise HTTPException(status_code=400, detail="Email already registered")

    new_user = UserModel(**user.dict())
    db.add(new_user)
    db.commit()

    db.refresh(new_user)

    return new_user

@router.get("/dashboard", response_model=User)
async def get_dashboard_data(tokenPayload: User = Depends(get_current_user)):
    
    return tokenPayload['user'];

@router.get("/users", response_model=List[User])
async def get_users(db: SessionLocal = Depends(get_db)):
    query = select(UserModel)
    users = db.execute(query).scalars().all()

    return users

@router.get("/delete_users")
async def delete_users(db: SessionLocal = Depends(get_db)):
    query = delete(UserModel)
    db.execute(query)
    db.commit()
    return {"message": "All users have been deleted"}